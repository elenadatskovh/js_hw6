//Опишите своими словами как работает цикл forEach.
//forEach выполняет прописанную функцию для каждого элемента массива,т.е.позволяет перебрать каждый элемент.



const data = ["hello", "world", 23, "23", null];

function filterBy(array) {
    const nullishValues = data.filter((item) => {
        return typeof item === "string";
    });
    const newArray = array.filter((value) => {
        return !nullishValues.includes(value);
    });
    return newArray;
}
console.log(filterBy(data));


